import { makeStyles } from '@material-ui/core/styles';

export default  makeStyles({
    card:{
        position: "relative"

    },
    overlay:{
        position: "absolute",
        top: '10px',
        left:'15px',
        color: "white",
        fontWeight: 'bold'

    },
    editIcon:{
        color:"white",
        position: "absolute",
        top:"10px",
        right:"15px"
    }

})