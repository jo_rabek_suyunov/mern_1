import { makeStyles } from '@material-ui/core/styles';

export default  makeStyles({
    paper:{
        padding: '25px',
        borderRadius: '30px'

    },
    buttonSubmit:{
        margin:'10px 0px'
    }

})