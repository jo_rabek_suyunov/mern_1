import React, {useState, useEffect} from 'react';
import {Container, AppBar, Typography, Grow, Grid} from '@material-ui/core';
import house from './components/images/2.jpg';
import Form from './components/Form/Form';
import Posts from './components/Posts/Posts';
import useStyles from './styles';
import {useDispatch} from 'react-redux';
import {getPosts} from "./redux/actions/posts";

const App = () => {
    const [currentId, setCurrentId] = useState(null);
    const classes = useStyles();
    const dispatch = useDispatch();

    console.log(dispatch(getPosts()))
    return (
        <Container maxWidth={'lg'}>
            <AppBar className={classes.appBar} position={'static'} color={'inherit'}>
                <Typography className={classes.heading} variant={'h2'} align={'center'}>Memorios</Typography>
                <img className={classes.image} src={house} alt="memorios" height={'60'}/>
            </AppBar>

            <Grid container className={classes.mainContainer} justify={'space-between'} alignItems={'stretch'}
                  spacing={3}>
                <Grid item xs={12} sm={7}>
                    <Posts setCurrentId={setCurrentId}/>
                    hello

                </Grid>
                <Grid item xs={12} sm={4}>
                    <Form currentId={currentId} setCurrentId={setCurrentId}/>
                </Grid>
            </Grid>

        </Container>

    )
}


export default App;