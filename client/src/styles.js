import {makeStyles, createMuiTheme, ThemeProvider, useTheme} from '@material-ui/core/styles';


export default makeStyles((theme) => ({

    appBar: {
        borderRadius: 15,
        margin: '30px 0',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    heading: {
        color: 'rgba(0, 183, 255, 1)',

    },
    image: {
        marginLeft: '15px'
    },
    [theme.breakpoints.down("md")]:{
        heading:{
            color: "green",
        }
    },
    [theme.breakpoints.down('sm')]:{
        heading:{
            fontSize: '41px',
            color:"red"
        },
        mainContainer:{
            flexDirection: 'column-reverse'
        }
    },

    //
    // [theme.breakpoints.down('xs')]:{
    //     appBar:{
    //         fontSize: '10px'
    //
    //     }
    // }


}))


export const myComponentSm = () => {

}