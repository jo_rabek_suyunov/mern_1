import * as api from '../api';
import * as types from './constants/actionTypes';


//Action creators
export const getPosts = () => async (dispatch) =>{
    try{
        const { data } = await api.fetchPosts();
        dispatch({ type: types.FETCH_ALL, payload: data });
    } catch (err){
        console.log(err.message)
    }
};


export const createPost = ( post ) => async ( dispatch ) => {
    try{
        const { data } = await api.createPost(post);
        dispatch( { type: types.CREATE, payload: data } )
    } catch (err){
        console.log( err )
    }
}


export const updatePost = ( id, post ) => async (dispatch) =>{
    try{
       const { data } = await api.updatePost( id, post );
       dispatch({ type: types.UPDATE, payload: data})
    }catch (err){
        console.log(err.message)
    }
};



export const deletePost = (id)=> async (dispatch) =>{
    try{
        await api.deletePost(id);
        dispatch({ type: types.DELETE, payload: id})
    }catch (e){
        console.log(e)
    }
}


export const likePost = (id) => async (dispatch) =>{
    try{
        const { data } = await  api.likePost(id);

        dispatch({ type: types.LIKE, payload: data })

    }catch(e){
        console.log(e)

    }
}
